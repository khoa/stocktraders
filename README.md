# Research project for stocktraders.vn

## Setup

To install all the dependencies/libraries.


```bash
conda env update -f environment.yml
```

## Running the script 

The input file will be `st_data.txt`.

The output file will be `st_out.csv`.

To run the script:

```
python calc_macd.py
```


