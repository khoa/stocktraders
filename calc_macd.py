import pandas as pd
from stockstats import StockDataFrame

if __name__ == '__main__':
    input_file = 'st_data.txt'
    output_file = 'st_out.csv'
    df = pd.read_csv(input_file, delimiter='\t')
    df['ticker'] = df['<Ticker>']
    df['open'] = df['<Open>']
    df['close'] = df['<Close>']
    df['high'] = df['<High>']
    df['low'] = df['<Low>']
    df['date'] = pd.to_datetime(df['<DTYYYYMMDD>'], format="%Y%m%d")
    df['volume'] = df['<Volume> ']
    df = df.drop(['<Ticker>', '<Open>', '<Close>', '<High>', '<Low>', '<Volume> ', '<DTYYYYMMDD>'], axis=1)
    tickers = df['ticker'].unique()
    print(df.head()) # print first 5 lines
    # print(df) # print all
    all_df = []
    i = 1
    deal_info = {}
    initial_money = 1e9
    for ticker in tickers:

        # filter ticker rows
        ticker_df = df[df.ticker==ticker]
        print(f'--- processing ticker {ticker}, len = {len(ticker_df)}')

        deal_info[ticker] = {
            'buy': 0,
            'sell': 0,
            'profit': 0
        }

        # sort excel file by date
        ticker_df = ticker_df.sort_values(by=['date'])

        # use the library
        ticker_df = StockDataFrame.retype(ticker_df)
        ticker_df['macd'] = ticker_df.get('macd')
        ticker_df['close_10_sma'] = ticker_df.get('close_10_sma')
        ticker_df['close_20_sma'] = ticker_df.get('close_20_sma')

        # use sma to set signal buy and sell
        prev_sma_10 = prev_sma_20 = None
        ticker_df['signal'] = None
        ticker_df['profit_percent'] = None
        prev_buy = None
        buy_amount = 0
        count_gain = 0
        count_loss = 0
        total_gain = 0
        total_loss = 0
        money = initial_money
        for index, row in ticker_df.iterrows():
            if prev_sma_10 is None:
                prev_sma_10 = row['close_10_sma']            
                prev_sma_20 = row['close_20_sma']
                continue
            else:
                if prev_sma_10 < prev_sma_20 and row['close_10_sma'] > row['close_20_sma']:
                    ticker_df.at[index, 'signal'] = 'buy'
                    deal_info[ticker]['buy'] += 1
                    buy_amount = money / row['close']
                    prev_buy = row['close']
                elif prev_sma_10 > prev_sma_20 and row['close_10_sma'] < row['close_20_sma']:
                    ticker_df.at[index, 'signal'] = 'sell'
                    deal_info[ticker]['sell'] += 1
                    if prev_buy is not None:
                        sell = row['close']
                        price_diff = sell - prev_buy
                        money_made = buy_amount * price_diff 
                        if price_diff > 0:
                            count_gain += 1
                            total_gain += money_made
                        else:
                            count_loss += 1
                            total_loss += money_made
                            
                        print(f'buy = {prev_buy:>10,.2f}, sell = {sell:>10,.2f}, price_diff = {price_diff:>10,.2f}, {"gain" if money_made>0 else "loss"} = {money_made:>20,.2f}')
                        deal_info[ticker]['profit'] += price_diff
                        ticker_df.at[index, 'profit_percent'] = price_diff * 100 / prev_buy
                prev_sma_10 = row['close_10_sma']            
                prev_sma_20 = row['close_20_sma']    
        gain_probability = 100 * total_gain / (total_gain + total_loss)
        total_trade = count_loss + count_gain
        avg_gain = total_gain/count_gain
        avg_loss = total_loss/count_loss
        print(f'total trade = {total_trade:,}, count gain = {count_gain}, count_loss = {count_loss}')
        print(f'total gain = {total_gain:>20,.2f}, avg gain = {avg_gain:>20,.2f}')
        print(f'total loss = {total_loss:>20,.2f}, avg loss = {avg_loss:>20,.2f}')
        print(f'avg win/loss = {avg_gain / abs(avg_loss):,.2f}')

        all_df.append(ticker_df)
        i -= 1
        if i<=0: break
    all_df = pd.concat(all_df)
    all_df.to_csv(output_file)
    print('done')
