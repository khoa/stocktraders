from collections import namedtuple, defaultdict

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from pathlib import Path

from recordtype import recordtype

NUM_DECIMAL = 2
DEBUG_LIMIT = 1
DEBUG = True
_debug_limit_count = 0

output_root = Path('output')
if not output_root.exists():
    output_root.mkdir()
df = pd.read_csv('DATA.txt')
all_tickers = df.Ticker.unique()
all_df = []
profit_df = []
Purchase = recordtype('Purchase', 'date price is_buy difference difference_ratio sma_a sma_b'.split())
ProfitRow = recordtype('ProfitRow', 'ticker profit_prob avg_profit avg_loss avg_profit_over_loss'.split())
smas = np.linspace(5, 150, num=30, dtype=np.int)


def get_key(sma_a, sma_b):
    """assume a<b"""
    return f'SMA_{sma_a}_{sma_b}'


for ticker in all_tickers:
    _debug_limit_count += 1
    if DEBUG and _debug_limit_count > DEBUG_LIMIT: break
    print(f'processing ticker {ticker}')
    df_purchase = []
    df_ticker = df[df.Ticker == ticker]
    df_ticker = df_ticker.set_index('Date')
    for sma in smas:
        df_ticker[f'SMA_{sma}'] = df_ticker['Close'].rolling(window=sma).mean()
        df_ticker[f'SMA_prev_{sma}'] = df_ticker[f'SMA_{sma}'].shift(1)
    purchases = defaultdict(lambda: defaultdict(list))
    last_buy_purchase = {}
    total_profit = defaultdict(lambda: defaultdict(float))
    total_trade = defaultdict(lambda: defaultdict(int))  # trade = 1 sell and 1 buy
    total_loss = defaultdict(lambda: defaultdict(float))
    count_profit = defaultdict(lambda: defaultdict(int))  # number of time a trade turns profit
    count_loss = defaultdict(lambda: defaultdict(int))  # number of time a trade turns loss
    avg_profit = defaultdict(lambda: defaultdict(float))
    avg_loss = defaultdict(lambda: defaultdict(float))
    profit_prob = defaultdict(lambda: defaultdict(float))
    avg_profit_over_loss = defaultdict(lambda: defaultdict(float))
    last_buy_purchase = defaultdict(lambda: defaultdict())
    for i_ticker, (index, row) in enumerate(df_ticker.iterrows()):
        if i_ticker == 0: continue
        for i in range(len(smas) - 1):
            sma_a = smas[i]
            for j in range(i + 1, len(smas)):
                sma_b = smas[j]
                sma_key = get_key(sma_a, sma_b)
                is_cross_up = row[f'SMA_prev_{sma_a}'] <= row[f'SMA_prev_{sma_b}'] and row[f'SMA_{sma_a}'] >= row[
                    f'SMA_{sma_b}']
                is_cross_down = row[f'SMA_prev_{sma_a}'] >= row[f'SMA_prev_{sma_b}'] and row[f'SMA_{sma_a}'] <= row[
                    f'SMA_{sma_b}']
                purchase = None
                if is_cross_up:  # buy when SMA_5 cross up with SMA_10
                    purchase = Purchase(date=index, price=row.Close, is_buy=True, difference=0,
                                        difference_ratio=0, sma_a=np.round(row[f'SMA_{sma_a}'], decimals=NUM_DECIMAL),
                                        sma_b=np.round(row[f'SMA_{sma_b}'], decimals=NUM_DECIMAL))
                    purchases[ticker][sma_key].append(purchase)
                    last_buy_purchase[ticker][sma_key] = purchase
                elif is_cross_down:  # sell when SMA_5 cross down with SMA_10
                    purchase = Purchase(date=index, price=row.Close, is_buy=False, difference=0,
                                        difference_ratio=0, sma_a=np.round(row[f'SMA_{sma_a}'], decimals=NUM_DECIMAL),
                                        sma_b=np.round(row[f'SMA_{sma_b}'], decimals=NUM_DECIMAL))
                    purchases[ticker][sma_key].append(purchase)

                if sma_key in last_buy_purchase[ticker] is not None and purchase is not None and \
                        last_buy_purchase[ticker][sma_key].is_buy and not purchase.is_buy:
                    total_trade[ticker][sma_key] += 1
                    profit = purchase.price - last_buy_purchase[ticker][sma_key].price
                    if profit > 0:
                        count_profit[ticker][sma_key] += 1
                        total_profit[ticker][sma_key] += profit
                    else:
                        count_loss[ticker][sma_key] += 1
                        total_loss[ticker][sma_key] += profit
                    purchase.difference = np.round(profit, decimals=NUM_DECIMAL)
                    purchase.difference_ratio = np.round(profit / last_buy_purchase[ticker][sma_key].price * 100)

    best_profit = 0
    best_sma = None
    for i in range(len(smas) - 1):
        sma_a = smas[i]
        for j in range(i + 1, len(smas)):
            sma_b = smas[j]
            sma_key = get_key(sma_a, sma_b)
            avg_profit[ticker][sma_key] = np.round(
                total_profit[ticker][sma_key] / count_profit[ticker][sma_key] if count_profit[ticker][
                                                                                     sma_key] > 0 else 0,
                decimals=NUM_DECIMAL)
            profit_prob[ticker][sma_key] = np.round(
                count_profit[ticker][sma_key] / total_trade[ticker][sma_key] if total_trade[ticker][sma_key] > 0 else 0,
                decimals=NUM_DECIMAL)
            avg_loss[ticker][sma_key] = np.round(
                abs(total_loss[ticker][sma_key] / count_loss[ticker][sma_key] if count_loss[ticker][
                                                                                     sma_key] > 0 else 0),
                decimals=NUM_DECIMAL)
            avg_profit_over_loss[ticker][sma_key] = np.round(
                avg_profit[ticker][sma_key] / avg_loss[ticker][sma_key] if avg_loss[ticker][sma_key] != 0 else 0,
                decimals=NUM_DECIMAL)
            avg_total_profit = profit_prob[ticker][sma_key] * avg_profit_over_loss[ticker][sma_key]
            if avg_total_profit > best_profit:
                best_profit = avg_total_profit
                best_sma = sma_key
            print(
                f'profit for {ticker}, sma = {sma_key}, profit prob = {profit_prob[ticker][sma_key]}, avg profit / avg loss = {avg_profit_over_loss[ticker][sma_key]}')
    print(f'best profit for ticker {ticker} = {best_profit}, sma = {best_sma}')
    # profit_row = ProfitRow(
    #     ticker=ticker,
    #     profit_prob=profit_prob[sma_key],
    #     avg_profit=avg_profit[sma_key],
    #     avg_loss=avg_loss[sma_key],
    #     avg_profit_over_loss=avg_profit_over_loss[sma_key]
    # )
    # profit_df.append(profit_row._asdict())
    # df_purchase = pd.DataFrame(list(purchase._asdict() for purchase in purchases))
    # df_purchase['Buy/Sell'] = df_purchase['is_buy'].map(lambda x: 'Buy' if x else 'Sell')
    # df_purchase['% Profit/Loss'] = df_purchase['difference_ratio'].map(lambda x: x if x != 0 else None)
    # df_purchase.drop(['difference_ratio', 'is_buy'], inplace=True, axis=1)
    # df_purchase = df_purchase.sort_index(ascending=False)
    # df_purchase.to_csv(str(output_root / f'{ticker}_purchases.csv'), index=False)
# all_df.append(df_ticker)
# plt.plot(df_ticker.Close)
# plt.plot(df_ticker.SMA_5)
# plt.plot(df_ticker.SMA_10)
# plt.show()

# result_df = pd.concat(all_df)
# result_df.to_csv('DATA_SMA5_SMA10.csv', index=False)
# profit_df = pd.DataFrame(profit_df)
# profit_df.to_csv('PROFIT_PROB.csv', index=False)
